import fetch from "node-fetch";

const serviceUrl = "http://localhost:1235";

const generateOptions = (body) => {
  return {
    method: "post",
    body: JSON.stringify(body),
    headers: { "Content-Type": "application/json" },
  };
};

const recordMessage = (startTimeMs, msgs, msg) => {
  msgs.push({
    time: +new Date() - startTimeMs,
    msg: msg,
  });
};

// Handle the requests asynchronously, don't do any blocking I/O
export async function requestAsync(ctx, sleepTimeMs) {
  const startTimeMs = +new Date();

  var msgs = [];
  recordMessage(startTimeMs, msgs, "running tasks asynchronously");

  // Kick off slowest task first
  var body = { sleepTimeMs: sleepTimeMs };
  const slow1 = fetch(serviceUrl + "/slow-task", generateOptions(body))
    .then((res) => res.json())
    .then((json) => recordMessage(startTimeMs, msgs, json.msg));

  // Kick off less slow task
  body = { sleepTimeMs: sleepTimeMs / 2 };
  const slow2 = fetch(serviceUrl + "/slow-task", generateOptions(body))
    .then((res) => res.json())
    .then((json) => recordMessage(startTimeMs, msgs, json.msg));

  // Kick off fast task after
  const fast = fetch(serviceUrl + "/fast-task")
    .then((res) => res.json())
    .then((json) => recordMessage(startTimeMs, msgs, json.msg));

  await fast;
  await slow1;
  await slow2;

  recordMessage(startTimeMs, msgs, "Finished gathering all messages.");
  ctx.body = msgs;
}

// Handle the requests asynchronously, don't do any blocking I/O and don't use promises
export async function requestAsyncNoPromise(ctx, sleepTimeMs) {
  const startTimeMs = +new Date();

  var msgs = [];
  recordMessage(
    startTimeMs,
    msgs,
    "running tasks asynchronously without promises"
  );

  // Kick off slow task first
  var body = { sleepTimeMs: sleepTimeMs };
  const slow1 = fetch(serviceUrl + "/slow-task", generateOptions(body));

  // Kick off less slow task
  body = { sleepTimeMs: sleepTimeMs / 2 };
  const slow2 = fetch(serviceUrl + "/slow-task", generateOptions(body));

  // Kick off fast task after
  const fast = fetch(serviceUrl + "/fast-task");

  // Now await all requests and handle their data
  const fastResp = await fast;
  const fastJson = await fastResp.json();
  recordMessage(startTimeMs, msgs, fastJson.msg);

  const slowResp1 = await slow1;
  const slowJson1 = await slowResp1.json();
  recordMessage(startTimeMs, msgs, slowJson1.msg);

  const slowResp2 = await slow2;
  const slowJson2 = await slowResp2.json();
  recordMessage(startTimeMs, msgs, slowJson2.msg);

  recordMessage(startTimeMs, msgs, "Finished gathering all messages.");

  ctx.body = msgs;
}

// Handle requests synchronously.
// Note: though this is blocking I/O, it does NOT block the event loop, it only blocks this particular call.
export async function requestSync(ctx, sleepTimeMs) {
  const startTimeMs = +new Date();

  var body, resp, data; // Just get some declarations out of the way

  var msgs = [];
  recordMessage(startTimeMs, msgs, "running tasks synchronously");

  // Do slowest task first (5 seconds)
  body = { sleepTimeMs: sleepTimeMs };
  resp = await fetch(serviceUrl + "/slow-task", generateOptions(body));
  data = await resp.json();
  recordMessage(startTimeMs, msgs, data.msg);

  // Do a less slow task (half as slow)
  body = { sleepTimeMs: sleepTimeMs / 2 };
  resp = await fetch(serviceUrl + "/slow-task", generateOptions(body));
  data = await resp.json();
  recordMessage(startTimeMs, msgs, data.msg);

  // Do fast task after
  resp = await fetch(serviceUrl + "/fast-task");
  data = await resp.json();
  recordMessage(startTimeMs, msgs, data.msg);

  recordMessage(startTimeMs, msgs, "Finished gathering all messages.");

  ctx.body = msgs;
}

export default {
  requestSync,
  requestAsync,
  requestAsyncNoPromise,
};
