import koa from "koa";
import Router from "@koa/router";
import bodyParser from "koa-bodyparser";

import { requestSync, requestAsync, requestAsyncNoPromise } from "./client.js";

const port = 1234;
const app = new koa();
const router = new Router();

app.use(bodyParser());

router.get("/", async (ctx, next) => {
  ctx.body = "App is running.";
});

// Endpoint to make requests to external service, but synchronously
router.get("/synchronous", async (ctx, next) => {
  await requestSync(ctx, 5000);
  await next();
});

// Endpoint to make requests to the external service, but asynchronously
router.get("/asynchronous", async (ctx, next) => {
  await requestAsync(ctx, 5000);
  await next();
});

// Endpoint to make requests to the external service, but asynchronously
router.get("/asynchronous2", async (ctx, next) => {
  await requestAsyncNoPromise(ctx, 5000);
  await next();
});

app.use(router.routes()).use(router.allowedMethods());

app.listen(port);

console.log(`App running on port ${port}...`);
