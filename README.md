# node-demo

## Pre-requisites
To run this yourself, you must have installed:  
- node - 14.17.4 used at the time of writing  
- yarn - 1.21.1 used at the time of writing  

Run `yarn` at the project root to install dependencies.  
In one terminal, run the mock service: `yarn mock`.  
In another terminal, run the app itself: `yarn dev`.  

Go to your browser, and you can any of the endpoints:  
- `/`
- `/synchronous`
- `/asynchronous`
- `/asynchronous2`

## Mock Service
Has two endpoints:

### `GET /fast-task`
Returns the following JSON body, all the time, and right away:

```
{
  msg: "fast task is done"
}
```

### `POST /slow-task`
Accepts a JSON body with a `sleepTimeMs` parameter:

```
{
  sleepTimeMs: [int]
}
```

The endpoint "sleeps" for the specified amount of milliseconds, and then returns the following body:

```
{
  msg: "slow task is done"
}
```
