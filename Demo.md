# What is NodeJS?
https://nodejs.org/en/about/

Some sample code:
https://nodejs.org/en/docs/guides/blocking-vs-non-blocking/

# The Event Loop
Not the focus of the presentation, but very relevant.  Instead, some resources:  
https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/  
https://nodejs.dev/learn/the-nodejs-event-loop  

A great talk with visualization of the event loop:
https://www.youtube.com/watch?v=8aGhZQkoFbQ

# Summary of Demo  
What I try to show here is the ease with which a developer can handle async i/o using NodeJS.  NodeJS allows us to code agnostically against async i/o, i.e. the code does not have to explicitly yield to the event loop, it does not have to manage its own "asynchronous-ness".  
- there are no thread pools to worry about, no threading  
- and therefore we dont have to worry about any locking  
- there is no database where we have to store currently running tasks (the event loop handles the call stack)  
- there's no recording of state, to make the function re-entrant and pick up from where it left off  
- there is nothing about our code that is even aware of an event loop, except for its ability to say "do it later"  

# Rich Ecosystem
- "there's a package for that", to the point of being ridiculous, like [this one](https://www.npmjs.com/package/is-odd).  

# Important Packages Used In Demo:
In case you want to dig deeper into some of the things done here...  
- node-fetch - https://github.com/node-fetch/node-fetch  
- koa - https://github.com/koajs/koa
- koa-router - https://github.com/koajs/router

# Drawbacks
- "callback hell" - nested callbacks, but async/await introduced some years ago mitigates this
- dependency management - similar to python packages, but exacerbated by how accessible/popular JS is. good processes in conjunction with auditing tools and self-hosting packages is the way to solve this

# Quirkiness
- wat: https://www.destroyallsoftware.com/talks/wat
- so Typescript is cool

# Other Resources
The library that implements NodeJS' event loop and other asynchronous behaviors is libuv: https://github.com/libuv/libuv

This guy is a really good/engaging teacher and has great tutorials: https://www.youtube.com/c/funfunfunction  

Here's a cool playlist on functional programming in JS:  
https://www.youtube.com/playlist?list=PL0zVEGEvSaeEd9hlmCXrk5yUyqUag-n84