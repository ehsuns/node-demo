import koa from "koa";
import Router from "@koa/router";
import bodyParser from "koa-bodyparser";

const port = 1235;
const app = new koa();
const router = new Router();

app.use(bodyParser());

const timer = (ms) => new Promise((res) => setTimeout(res, ms));

router.get("/", async (ctx, next) => {
  ctx.body = "Mock external service is up.";
});

router.get("/fast-task", async (ctx, next) => {
  ctx.body = { msg: "fast task is done" };
});

router.post("/slow-task", async (ctx, next) => {
  const d = ctx.request.body;
  console.log(`sleeping for ${d.sleepTimeMs} ms`);
  await timer(d.sleepTimeMs);
  ctx.body = { msg: `slow task ${d.sleepTimeMs} is done` };

  await next();
});

app.use(router.routes()).use(router.allowedMethods());

app.listen(port);

console.log(`External service running on port ${port}...`);
